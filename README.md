# Dormitory Management System

Проект по реализации управления общежитиями


Весь проект разварачивался на Yandex Cloud k8s кластере, манифесты можно увидеть в папке K8s-manifests.

Что было сделано:
- Прописаны deploy и service для BE(backend), FE(frontend\client), crawler, mongo-db частей приложения
- Так как основной доступ для пользователей ко всему сервису осуществлялся через FE, то также прописан ingress
- Отдельно credentials и Persistent Volume Claim для БД Mongo-db
- Был настроен nginx
- Для каждого сервиса был реализована CI pipline 
- Был использован собственно созданный в YC Container registry, вместо docker-hub-a для FE
- Добавлен docker-compose для локального тестирования


Одна из проблем, которую долго пришлось фиксить - YC при создании PV делает его "использованным", что не давало возможности присоеденить к нему PVC.

## Запуск 

1. Клонировать репозиторий и все сервисы

`git clone https://gitlab.com/bzm_blog/general.git`

`git submodule update --init --recursive`

2. Собрать образ для FE и запушить на удобный для Вас Container registry

3. Поменять CR в K8s-manifests/client/deploy-client-v3.yml
    `image: cr.yandex/crplamltq8fpeica9ec9/client:v3`

на тот, куда Вы запушили собранный клиент

4. Применить каждый манифест на k8s кластер с помощю команды 
`kubectl apply -f [название манифеста]`

